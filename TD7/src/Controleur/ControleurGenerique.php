<?php

namespace App\Covoiturage\Controleur;

class ControleurGenerique
{
    public static function afficherVue(string $cheminVue, array $parametres = []): void {
        extract($parametres);
        require __DIR__ . "/../vue/$cheminVue";
    }
    public static function afficherErreur(string $messageErreur = ""): void {
        if ($messageErreur === "") {
            $messageErreur = "Erreur inconnue.";
        } else {
            $messageErreur = "Erreur : " . htmlspecialchars($messageErreur);
        }
        self::afficherVue('vueGenerale.php', [
            "messageErreur" => $messageErreur,
            "titre" => "Erreur",
            "cheminCorpsVue" => "/../vue/erreur.php"]);
    }
    public static function afficherFormulairePreference(): void {
        self::afficherVue('vueGenerale.php', [
            "message" =>    "La préférence a été enregistrée",
            "titre" => "Préférences",
            "cheminCorpsVue" => "/../vue/formulairePreference.php"]);
    }
    public static function enregistrerPreference(): void {
        if (isset($_GET['controleur_defaut'])) {
            \App\Covoiturage\Lib\PreferenceControleur::enregistrer($_GET['controleur_defaut']);
            self::afficherVue('vueGenerale.php', ["message" => "La préférence de contrôleur est enregistrée !", "titre" => "Préférence enregistrée", "cheminCorpsVue" => "/../vue/preferenceEnregistree.php"]);
        } else {
            self::afficherErreur("Aucune préférence sélectionnée.");
        }
    }
}