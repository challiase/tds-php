<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\HTTP\Cookie as Cookie;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateur = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Liste" ,"cheminCorpsVue" => "/../vue/utilisateur/liste.php"]);
    }
    public static function afficherDetail() : void
    {
        if (!isset($_GET['login']))
            self::afficherErreur("L'utilisateur avec le login spécifié est introuvable.");

        else {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
            if (empty($utilisateur))
                self::afficherErreur("L'utilisateur avec le login spécifié est introuvable.");

            else {
                self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Détail", "cheminCorpsVue" => "/../vue/utilisateur/detail.php"]);
            }
        }
    }

    public static function afficherFormulaireDeCreation() : void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire", "cheminCorpsVue" => "/../vue/utilisateur/formulaireCreation.php"]);
    }
    public static function creerDepuisFormulaire() : void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        if (!isset($utilisateur)) {
            self::afficherErreur("Impossible d'ajouter un utilisateur, (Ce nom est deja pris)");
        }
        else{
            self::afficherVue('vueGenerale.php', ["utilisateur" => $tableauUtilisateur,"login" => $utilisateur->getLogin(), "titre" => "Liste" ,"cheminCorpsVue" => "/../vue/utilisateur/utilisateurCree.php"]);
        }
    }

    /**
     * @return void
     * @throws \DateMalformedStringException
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $utilisateur = new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['prenom'], $tableauDonneesFormulaire['nom']);
        return $utilisateur;
    }

    public static function afficherUtilisateurSupprime(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        (new UtilisateurRepository())->supprimer(($_GET['login']));
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["login" => $utilisateur->getLogin(), "utilisateur" => $tableauUtilisateur ,"titre" => "Utilisateur supprimer", "cheminCorpsVue" => "/../vue/utilisateur/utilisateurSupprime.php"]);
    }
    public static function afficherFormulaireMiseAJour(): void
    {
        if (!isset($_GET['login'])) {
            echo "Erreur : aucun login fourni.";
            return;
        }
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur === null) {
            echo "Erreur : utilisateur non trouvé.";
            return;
        }
        ControleurGenerique::afficherVue('vueGenerale.php', [
            'utilisateur' => $utilisateur,
            'cheminCorpsVue' => '/../vue/utilisateur/formulaireMiseAJour.php'
        ]);
    }

    public static function mettreAJour() : void {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $tableauUtilisateur = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["login" => $utilisateur->getLogin(), "utilisateur" => $tableauUtilisateur, "titre" => "Utilisateur mis à jour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }
    public static function deposerCookie() : void
    {
        $utilisateur = new Utilisateur("AgatheZeblouse", "Zeblouse", "Agathe");
        Cookie::enregistrer('utilisateur', $utilisateur, time() + (30 * 24 * 60 * 60));
        self::afficherVue('vueGenerale.php', [
            "message" => "Le cookie a été déposé avec succès.",
            "titre" => "Cookie déposé",
            "cheminCorpsVue" => "/../vue/utilisateur/cookieDepose.php"
        ]);
    }
    public static function lireCookie(): void
    {
        if (Cookie::contient('utilisateur')) {
            $utilisateur = Cookie::lire('utilisateur');
            $message = "Le cookie 'utilisateur' contient : " . $utilisateur;
        } else {
            $message = "Le cookie 'utilisateur' n'existe pas ou a expiré.";
        }

        self::afficherVue('vueGenerale.php', [
            "message" => $message,
            "titre" => "Lire Cookie",
            "cheminCorpsVue" => "/../vue/utilisateur/lireCookie.php"
        ]);
    }
    public static function supprimerCookie(): void
    {
        Cookie::supprimer('utilisateur');
        self::afficherVue('vueGenerale.php', [
            "message" => "Le cookie 'utilisateur' a été supprimé.",
            "titre" => "Cookie supprimé",
            "cheminCorpsVue" => "/../vue/utilisateur/cookieSupprime.php"
        ]);
    }
    public static function testerSession(): void
    {
        $session = \App\Covoiturage\Modele\HTTP\Session::getInstance();

        $session->enregistrer('utilisateur', 'Cathy Penneflamme');
        $session->enregistrer('panier', ['produit1', 'produit2', 'produit3']);
        $session->enregistrer('derniereConnexion', new \DateTime());

        echo "Utilisateur : " . $session->lire('utilisateur') . "<br>";
        echo "Panier : ";
        print_r($session->lire('panier'));
        echo "<br>";
        echo "Dernière connexion : ";
        print_r($session->lire('derniereConnexion'));
        echo "<br>";

        if ($session->contient('utilisateur')) {
            echo "L'utilisateur est dans la session.<br>";
        }
        $session->supprimer('panier');
        if (!$session->contient('panier')) {
            echo "Le panier a été supprimé de la session.<br>";
        }
        $session->detruire();
        echo "La session a été détruite.";
    }

    public static function testerSessionExpiration(): void
    {
        try {
            $session = \App\Covoiturage\Modele\HTTP\Session::getInstance();
            $session->enregistrer('utilisateur', 'Cathy Penneflamme');
            echo "Utilisateur enregistré : " . $session->lire('utilisateur') . "<br>";
            echo "Attendez la période d'expiration pour voir si la session se détruit.";
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }


}