<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository as TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use \DateTime;

class ControleurTrajet extends ControleurGenerique
{
    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["trajets" => $trajets, "titre" => "Liste" ,"cheminCorpsVue" => "/../vue/trajet/liste.php"]);
    }
    public static function afficherDetail() : void
    {
        if (!isset($_GET['trajet']))
            self::afficherErreur("Le trajet avec l'id spécifié est introuvable.");

        else {
            $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['trajet']);
            if (empty($trajet))
                self::afficherErreur("Le trajet avec l'id spécifié est introuvable.");

            else {
                self::afficherVue('vueGenerale.php', ["trajet" => $trajet, "titre" => "Détail", "cheminCorpsVue" => "/../vue/trajet/detail.php"]);
            }
        }
    }

    public static function supprimer() : void {
        if(isset($_GET["id"])) {
            (new TrajetRepository())->supprimer($_GET["id"]);
        }
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["trajets" => $trajets, "id" => $_GET["id"], "titre" => "Trajet supprimé", "cheminCorpsVue" => "trajet/trajetSupprime.php"]);
    }

    public static function afficherFormulaireDeCreation() : void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire", "cheminCorpsVue" => "/../vue/trajet/formulaireDeCreation.php"]);
    }


    public static function creerDepuisFormulaire() : void {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["trajet" => $_GET['id'],"trajets" => $trajets, "titre" => "Trajet créé", "cheminCorpsVue" => "trajet/trajetCree.php"]);
    }

    /**
     * @return void
     * @throws \DateMalformedStringException
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $nonFumeur = $tableauDonneesFormulaire["nonFumeur"] ?? false;
        $trajet = new Trajet($id, $tableauDonneesFormulaire['depart'], $tableauDonneesFormulaire['arrivee'], new DateTime($tableauDonneesFormulaire['date']), $tableauDonneesFormulaire['prix'], (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire['conducteurLogin']), $nonFumeur);
        return $trajet;
    }

    public static function afficherFormulaireMiseAJour() : void {
        if(isset($_GET["id"])) {
            $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET["id"]);
        }
        self::afficherVue("vueGenerale.php", ["titre" => "Formulaire mise à jour", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php", "trajet" => $trajet]);
    }

    public static function mettreAJour() : void {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["id" => $trajet->getId(), "trajets" => $trajets, "titre" => "Trajet mis à jour", "cheminCorpsVue" => "trajet/trajetMiseAJour.php"]);
    }








}