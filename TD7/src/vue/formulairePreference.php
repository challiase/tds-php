<h1>Choisissez votre contrôleur par défaut</h1>
<form action="controleurFrontal.php" method="get">
    <?php
    $preference = \App\Covoiturage\Lib\PreferenceControleur::lire();
    ?>
    <input type="hidden" name="action" value='enregistrerPreference'>

    <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?= $preference === 'utilisateur' ? 'checked' : '' ?>>
    <label for="utilisateurId">Utilisateur</label><br>

    <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?= $preference === 'trajet' ? 'checked' : '' ?>>
    <label for="trajetId">Trajet</label><br>

    <button type="submit">Enregistrer</button>
</form>
