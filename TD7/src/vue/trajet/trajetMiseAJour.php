<?php
/**
 * @var string $id Le login de l'utilisateur mis à jour
 * @var array $listeUtilisateurs La liste des utilisateurs actualisée
 */
?>

<h1>Mise à jour réussie</h1>

<p>Le trajet avec l'id <strong><?= htmlspecialchars($id) ?></strong> a bien été mis à jour.</p>

<?php include 'liste.php'; ?>
