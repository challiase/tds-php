<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php

use App\Covoiturage\Modele\DataObject\Trajet as Trajet;

echo "<h1>Liste des trajets</h1>";
/** @var Trajet[] $trajets */
foreach ($trajets as $trajet){
    $iDHTML = htmlspecialchars($trajet->getId());
    $iDURL = rawurlencode($trajet->getId());
    echo '<ol> <p> Trajet avec id ' . $iDHTML . '<a href="controleurFrontal.php?action=afficherDetail&controleur=trajet&trajet=' .  $iDURL . '">' . "  (+ d'info)" . '</a> ' .
         '<a href="controleurFrontal.php?action=supprimer&controleur=trajet&id=' . $iDURL . '">' . "  (supprimer)" . '</a>' .
         '<a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=trajet&id=' . $iDURL . '">' . "  (modifier)" . '</a> </p> </ol>';
}
echo '<p> <a href="controleurFrontal.php?action=afficherFormulaireDeCreation&controleur=trajet"> Créer un Trajet </a> </p>';
?>
</body>
</html>
