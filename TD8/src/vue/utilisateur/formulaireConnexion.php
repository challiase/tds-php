<h1>Connexion</h1>

<form action="controleurFrontal.php" method="get">

    <!-- Champ caché pour spécifier l'action de mise à jour -->
    <input type="hidden" name="action" value="connecter">
    <input type="hidden" name="controleur" value="utilisateur">

    <!-- Champ pour le login, readonly pour empêcher sa modification -->
    <label for="idLogin">Login :</label>
    <input type="text" id="idLogin" name="login" value=""><br><br>

    <!-- Champ pour le nouveau mot de passe -->
    <label for="idMdp">Nouveau mot de passe :</label>
    <input type="password" id="idMdp" name="mdp" required><br><br>

    <button type="submit">Se connecter</button>
</form>