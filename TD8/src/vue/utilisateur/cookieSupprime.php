<?php
/**
 * @var string $titre
 * @var string $message
 */
echo "<h1>" . htmlspecialchars($titre) . "</h1>";
echo "<p>" . htmlspecialchars($message) . "</p>";