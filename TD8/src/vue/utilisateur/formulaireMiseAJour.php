<?php
/**
 * @var Utilisateur $utilisateur L'objet Utilisateur dont les données sont à mettre à jour
 */

use App\Covoiturage\Modele\DataObject\Utilisateur;

?>

<h1>Mettre à jour les informations de l'utilisateur</h1>

<form action="controleurFrontal.php" method="get">

    <!-- Champ caché pour spécifier l'action de mise à jour -->
    <input type="hidden" name="action" value="mettreAJour">
    <input type="hidden" name="controleur" value="utilisateur">

    <!-- Champ pour le login, readonly pour empêcher sa modification -->
    <label for="login">Login :</label>
    <input type="text" id="login" name="login" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" readonly><br><br>

    <!-- Champ pour le nom -->
    <label for="nom">Nom :</label>
    <input type="text" id="nom" name="nom" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" required><br><br>

    <!-- Champ pour le prénom -->
    <label for="prenom">Prénom :</label>
    <input type="text" id="prenom" name="prenom" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" required><br><br>

    <label for="ancienMdp">Ancien mot de passe :</label>
    <input type="password" id="ancienMdp" name="ancienMdp" required><br><br>

    <!-- Champ pour le nouveau mot de passe -->
    <label for="nouveauMdp">Nouveau mot de passe :</label>
    <input type="password" id="nouveauMdp" name="mdp" required><br><br>

    <!-- Champ pour confirmer le nouveau mot de passe -->
    <label for="nouveauMdp2">Confirmer le nouveau mot de passe :</label>
    <input type="password" id="nouveauMdp2" name="nouveauMdp2" required><br><br>

    <button type="submit">Mettre à jour</button>
</form>
