<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> form </title>
</head>

<body>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>

        <input type='hidden' name='action' value='creerDepuisFormulaire'>
        <input type='hidden' name='controleur' value='trajet'>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Départ</label>
            <input class="InputAddOn-field" type="text" placeholder="Montpellier" name="depart" id="depart_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée</label>
            <input class="InputAddOn-field" type="text" placeholder="Sète" name="arrivee" id="arrivee_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date</label>
            <input class="InputAddOn-field" type="date" placeholder="marc" name="date" id="date_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix</label>
            <input class="InputAddOn-field" type="text" placeholder="marc" name="prix" id="prix_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="loginConducteur_id">Login du conducteur</label>
            <input class="InputAddOn-field" type="text" placeholder="marc" name="conducteurLogin" id="loginConducteur_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="fumeur_id">Accepte fumeur </label>
            <input class="InputAddOn-field" type="checkbox" name="fumeur" id="fumeur_id"/>
        </p>

        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>