<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../ressources/style.css">
    <meta charset="UTF-8">
    <title><?php
        /**
         * @var string $titre
         */
        echo $titre; ?>
    </title>
</head>
<body>
<header>
     <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/img/coeur.png" alt="Préférences" />Préférences</a>
            </li>
            <?php
            use \App\Covoiturage\Lib\ConnexionUtilisateur as ConnexionUtilisateur;
            use App\Covoiturage\Modele\Repository\UtilisateurRepository;

            if (!ConnexionUtilisateur::estConnecte()) {
                echo '
                    <li>
                        <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireDeCreation"><img src="../ressources/img/add-user.png" alt="Inscription">Inscription</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireConnexion"><img src="../ressources/img/enter.png" alt="Connexion">Connexion</a>
                    </li>';
            }
            else {
                $login = ConnexionUtilisateur::getLoginUtilisateurConnecte();
                echo '
                    <li>
                        <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login='. rawurlencode($login) .'"> <img src="../ressources/img/add-user.png" alt="Détails">Détails</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?controleur=utilisateur&action=deconnecter"><img src="../ressources/img/enter.png" alt="Déconnexion">Déconnexion</a>
                    </li>';
            }
            ?>
        </ul>
    </nav>
</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Evan
    </p>
</footer>
</body>
</html>