<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Cookie;

class PreferenceControleur
{
    private static string $clePreference = "preferenceControleur";

    public static function enregistrer(string $preference): void {
        Cookie::enregistrer(self::$clePreference, $preference, time() + (30 * 24 * 60 * 60)); // Cookie de 30 jours
    }

    public static function lire(): string {
        return Cookie::lire(self::$clePreference) ?? 'utilisateur';  // Valeur par défaut 'utilisateur'
    }

    public static function existe(): bool {
        return Cookie::contient(self::$clePreference);
    }

    public static function supprimer(): void {
        Cookie::supprimer(self::$clePreference);
    }
}