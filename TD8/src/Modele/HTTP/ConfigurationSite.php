<?php

namespace App\Covoiturage\Modele\HTTP;

class ConfigurationSite
{
    private static int $dureeExpirationSession = 1800; // 30 minutes = 1800 secondes

    public static function getDureeExpirationSession(): int {
        return self::$dureeExpirationSession;
    }


}
