<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

use App\Covoiturage\Controleur\ControleurUtilisateur as ControleurUtilisateur;
use App\Covoiturage\Lib\PreferenceControleur as PreferenceControleur;


$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


if (isset($_GET['controleur']))
{
    $controleur = $_GET['controleur'];
}
else
{
    if (PreferenceControleur::existe()) {
        $controleur = PreferenceControleur::lire();
    } else {
        $controleur = 'utilisateur';
    }
}


$action = $_GET['action'] ?? 'afficherListe';

$nomDeClasseControleur = "App\\Covoiturage\\Controleur\\Controleur" . ucfirst($controleur);

if (class_exists($nomDeClasseControleur)) {
    $controleurInstance = new $nomDeClasseControleur();

    // Vérification si l'action demandée existe dans ce contrôleur
    if (in_array($action, get_class_methods($nomDeClasseControleur))) {
        $controleurInstance->$action();
    } else {
        ControleurUtilisateur::afficherErreur("Action '$action' non trouvée dans le contrôleur $controleur.");
    }
} else {
    ControleurUtilisateur::afficherErreur("Contrôleur '$controleur' non trouvé.");
}
