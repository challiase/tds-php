<?php

require_once('ConnexionBaseDeDonnees.php');

class Utilisateur
{

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login): void
    {
        $this->login = substr($login, 0, 64);
    }

    // un constructeur
    public function __construct(string $login, string $nom, string $prenom)
    {
        $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string
    {
        return "Login : " . $this->login . ", Nom : " . $this->nom . ", Prenom : " . $this->prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public static function recupererUtilisateur(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $listeUtilisateur = [];
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateur = self::construireDepuisTableauSQL($utilisateurFormatTableau);
            $listeUtilisateur[] = $utilisateur;
        }
        return $listeUtilisateur;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginBaseDeDonnees";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginBaseDeDonnees" => $login,
        );

        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau === false) {
            return null;
        }
        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void
    {
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "login" => $this->getLogin(),
            "nom" => $this->getNom(),
            "prenom" => $this->getPrenom(),
        );
        $pdoStatement->execute($values);
    }

    public function supprimer() : void
    {
        $sql = "DELETE FROM utilisateur WHERE login = :loginBaseDeDonnees";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "login" => $this->getLogin(),
        );
        $pdoStatement->execute($values);
    }
}
?>