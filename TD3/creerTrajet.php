<?php
require_once("Trajet.php");

echo  "<pre>";
print_r($_POST);
echo "</pre>";


$tableauTrajet = array (
    'id' => null,
    'depart' => $_POST['depart'] ?? null,
    'arrivee' => $_POST['arrivee'] ?? null,
    'date' => $_POST['date'] ?? null,
    'prix' => $_POST['prix'] ?? null,
    'conducteurLogin' => $_POST['conducteurLogin'] ?? null,
    'nonFumeur' => isset($_POST['nonFumeur']) ? true : false
);

if (!empty($tableauTrajet['depart']) && !empty($tableauTrajet['arrivee']) && !empty($tableauTrajet['date']) && !empty($tableauTrajet['prix']) && !empty($tableauTrajet['conducteurLogin'])) {
    // Build the Trajet object
    try {
        $trajet = Trajet::construireDepuisTableauSQL($tableauTrajet);
        $trajet->ajouter();
        echo $trajet->__tostring();
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
} else {
    echo "Error: Missing required fields.";
}