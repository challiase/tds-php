<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateur = ModeleUtilisateur::recupererUtilisateur(); //appel au modèle pour gérer la BD
        //require ('../vue/utilisateur/liste.php');  //"redirige" vers la vue
        //self::afficherVue('utilisateur/liste.php', ["utilisateur" => $utilisateur]);
        self::afficherVue('utilisateur/vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Liste" ,"cheminCorpsVue" => "liste.php"]);
    }
    public static function afficherDetail() : void
    {
        if (!isset($_GET['login'])) self::afficherVue('utilisateur/vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "erreur.php"]);

        else {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
            if (empty($utilisateur)) self::afficherVue('utilisateur/vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "erreur.php"]);

            else {
                self::afficherVue('utilisateur/vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Détail", "cheminCorpsVue" => "detail.php"]);
            }
        }
    }

    public static function afficherFormulaireDeCreation() : void
    {
        self::afficherVue('utilisateur/vueGenerale.php', ["titre" => "Formulaire", "cheminCorpsVue" => "formulaireCreation.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function creerDepuisFormulaire()
    {
        $login = isset($_GET['login']) ? $_GET['login'] : '';
        $prenom = isset($_GET['prenom']) ? $_GET['prenom'] : '';
        $nom = isset($_GET['nom']) ? $_GET['nom'] : '';

        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        //self::afficherListe();
        $tableauUtilisateur = ModeleUtilisateur::recupererUtilisateur();
        self::afficherVue('utilisateur/utilisateurCree.php', ["utilisateur" => $tableauUtilisateur,"login" => $utilisateur->getLogin(), "titre" => "Liste" ,"cheminCorpsVue" => "utilisateurCree.php"]);
    }
}
?>
