<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
echo "<h1>Liste des utilisateurs</h1>";
/** @var ModeleUtilisateur[] $utilisateur */
foreach ($utilisateur as $utilisateur){
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<ol> <p> Utilisateur de login ' . $loginHTML . '<a href="controleurFrontal.php?action=afficherDetail&login=' .  $loginURL . '">' . " (+ d'info)" . '</a> </p> </ol>';
}
echo '<p> <a href="controleurFrontal.php?action=afficherFormulaireDeCreation"> Créer un utilisateur </a> </p>';
?>
</body>
</html>
