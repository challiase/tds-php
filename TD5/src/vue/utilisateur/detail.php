<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur $utilisateur */
echo '<p> Utilisateur de login ' . htmlspecialchars($utilisateur->getLogin()) . '.</p>';
echo '<p> Utilisateur de nom ' . htmlspecialchars($utilisateur->getNom()) . '.</p>';
echo '<p> Utilisateur de prenom ' . htmlspecialchars($utilisateur->getPrenom()) . '.</p>';
?>

</body>
</html>
