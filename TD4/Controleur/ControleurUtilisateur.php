<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateur = ModeleUtilisateur::recupererUtilisateur(); //appel au modèle pour gérer la BD
        //require ('../vue/utilisateur/liste.php');  //"redirige" vers la vue
        self::afficherVue('utilisateur/liste.php', ["utilisateur" => $utilisateur]);
    }
    public static function afficherDetail() : void
    {
        if (!isset($_GET['login'])) self::afficherVue('utilisateur/erreur.php');

        else {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
            if (empty($utilisateur)) self::afficherVue('utilisateur/erreur.php');

            else {
                self::afficherVue('utilisateur/detail.php', ["utilisateur" => $utilisateur]);
            }
        }
    }

    public static function afficherFormulaireDeCreation() : void
    {
        self::afficherVue("utilisateur/formulaireCreation.php");
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function creerDepuisFormulaire()
    {
        $login = isset($_GET['login']) ? $_GET['login'] : '';
        $prenom = isset($_GET['prenom']) ? $_GET['prenom'] : '';
        $nom = isset($_GET['nom']) ? $_GET['nom'] : '';

        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        self::afficherListe();
    }

}
?>
