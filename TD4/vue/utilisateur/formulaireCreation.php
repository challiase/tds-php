<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> form </title>
</head>

<body>
<form method="get" action="routeur.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <input type='hidden' name='action' value='creerDepuisFormulaire'>

            <label for="login_id">Login</label> :
            <input type="text" placeholder="MarcAssin" name="login" id="login_id" required/>

            <label for="nom_id">Nom</label> :
            <input type="text" placeholder="assin" name="nom" id="nom_id" required/>

            <label for="prenom_id">Prénom</label> :
            <input type="text" placeholder="marc" name="prenom" id="prenom_id" required/>
        </p>

        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>