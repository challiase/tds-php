<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;

          $nom = "Challias";
          $prenom = "Evan";
          $login = "tongue";
          echo "<p> Utilisateur : $nom $prenom de login $login </p>";

          $utilisateurs = [
           [
                   'nom' => $nom,
                  'prenom' => $prenom,
                  'login' => $login
          ],
          [
                  'nom' => 'Tongue',
                  'prenom' => 'Chaude',
                  'login' => 'tonguechaude'
          ],
        ];

          //echo "<p>Utilisateur : $utilisateurs[nom] $utilisateurs[prenom] de login $utilisateurs[login]</p>";

          //var_dump($utilisateur);
          //print_r($utilisateur);
        if (empty($utilisateurs)) {
            echo '<p>Il n\'y a aucun utilisateur.</p>';
        }
        else {
            echo '<h1>Liste des utilisateurs :</h1>';
            echo '<ul>';
            foreach ($utilisateurs as $utilisateur) {
                echo '<li>';
                echo '<b>Nom :</b> ' . $utilisateur['nom'] . '<br>';
                echo '<b>Prénom :</b> ' . $utilisateur['prenom'] . '<br>';
                echo '<b>Login :</b> ' . $utilisateur['login'];
                echo '</li>' . '<br>';
            }
            echo '</ul>';
        }
        ?>
    </body>
</html> 