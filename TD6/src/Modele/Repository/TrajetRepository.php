<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use \DateTime;
class TrajetRepository extends AbstractRepository
{
    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        return new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"],
        );

    }

    protected static function recupererPassagers(Trajet $trajet): array
    {
        $sql = "SELECT u.login, u.nom, u.prenom
                FROM utilisateur u
                JOIN trajet t ON u.login = t.login";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array (
            'trajetId' => $trajet->getId()
        );
        $pdoStatement->execute($values);

        $listePassagers = [];
        foreach ($pdoStatement as $passagerFormatTableau) {
            $passager = (new UtilisateurRepository())->construireDepuisTableauSQL($passagerFormatTableau);
        }

        return $listePassagers;
    }

    protected function getNomTable() : string
    {
        return "trajet";
    }

    protected function getNomClePrimaire() : string
    {
        return "id";
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }
    protected function formatTableauSQL(AbstractDataObject $trajet): array {
        /** @var Trajet $trajet */
        return [
            'id'=>$trajet->getId(),
            'depart' => $trajet->getDepart(),
            'arrivee' => $trajet->getArrivee(),
            'date' => $trajet->getDate()->format('Y-m-d'), // Conversion DateTime en string
            'prix' => $trajet->getPrix(),
            'conducteurLogin' => $trajet->getConducteur()->getLogin(),
            'nonFumeur' => $trajet->isNonFumeur() ? 1 : 0, // Conversion bool en int
        ];
    }

}