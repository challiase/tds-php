<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php

use App\Covoiturage\Modele\DataObject\Utilisateur as Utilisateur;

echo "<h1>Liste des utilisateurs</h1>";
/** @var Utilisateur[] $utilisateur */
foreach ($utilisateur as $utilisateur){
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<ol> <p> Utilisateur de login ' . $loginHTML . '<a href="controleurFrontal.php?action=afficherDetail&login=' .  $loginURL . '">' . "  (+ d'info)" . '</a> ' .
         '<a href="controleurFrontal.php?action=afficherUtilisateurSupprime&login=' . $loginURL . '">' . "  (supprimer)" . '</a>' .
         '<a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&login=' . $loginURL . '">' . "  (modifier)" . '</a> </p> </ol>';
}
echo '<p> <a href="controleurFrontal.php?action=afficherFormulaireDeCreation"> Créer un utilisateur </a> </p>';
?>
</body>
</html>
