<?php
/**
 * @var string $login Le login de l'utilisateur mis à jour
 * @var array $listeUtilisateurs La liste des utilisateurs actualisée
 */
?>

<h1>Mise à jour réussie</h1>

<p>L’utilisateur de login <strong><?= htmlspecialchars($login) ?></strong> a bien été mis à jour.</p>

<?php include 'liste.php'; ?>
