<?php
/**
 * @var Trajet $trajet  L'objet $trajet dont les données sont à mettre à jour
 */

use App\Covoiturage\Modele\DataObject\Trajet;

?>

<h1>Mettre à jour les informations du trajet</h1>

<form action="controleurFrontal.php" method="get">

    <!-- Champ caché pour spécifier l'action de mise à jour -->
    <input type="hidden" name="action" value="mettreAJour">
    <input type='hidden' name='controleur' value='trajet'>


    <label for="id">Id :</label>
    <input type="text" id="id" name="id" value="<?= htmlspecialchars($trajet->getId()) ?>" readonly><br><br>

    <label for="depart">Départ :</label>
    <input type="text" id="depart" name="depart" value="<?= htmlspecialchars($trajet->getDepart()) ?>" required><br><br>

    <label for="arrivee">Arrivée :</label>
    <input type="text" id="arrivee" name="arrivee" value="<?= htmlspecialchars($trajet->getArrivee()) ?>" required><br><br>

    <label for="date">Date :</label>
    <input type="date" id="date" name="date" value="<?= htmlspecialchars($trajet->getDate()->format("y-m-d")) ?>"><br><br>

    <label for="prix">Prix :</label>
    <input type="text" id="prix" name="prix" value="<?= htmlspecialchars($trajet->getPrix()) ?>" required><br><br>

    <label for="conducteurLogin">Login conducteur :</label>
    <input type="text" id="conducteurLogin" name="conducteurLogin" value="<?= htmlspecialchars($trajet->getConducteur()->getLogin()) ?>" required><br><br>

    <label for="fumeur">Fumeur autorisée :</label>
    <input type="checkbox" id="fumeur" name="fumeur" value="<?= htmlspecialchars($trajet->isNonFumeur()) ?>"><br><br>

    <button type="submit">Mettre à jour</button>
</form>
