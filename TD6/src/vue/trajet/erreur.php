<?php
/**
 * @var string $messageErreur
 */
if (!empty($messageErreur)) {
    echo "<p>" . $messageErreur . "</p>";
} else {
    echo "<p>Problème avec le trajet.</p>\n";
}
?>
